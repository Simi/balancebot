$fn = 100;

//stepper motor
motor_size = 42;
motor_screw_distance = 31;
motor_screw = 3;
motor_width = 7;
motor_center = 22;
motor_spacing = 3;
motor_length = 33.5;

//arduino mega
board_offset = 10;
board_length = 110;
board_width = 54;
board_screw_a_x = 14 + board_offset;
board_screw_a_y = 2.5 + board_offset;
board_screw_b_x = 66 + board_offset;
board_screw_b_y = 7.5 + board_offset;
board_screw_c_x = 96.5 + board_offset;
board_screw_c_y = 2.5 + board_offset;
board_screw_d_x = 15 + board_offset;
board_screw_d_y = 51 + board_offset;
board_screw_e_x = 90 + board_offset;
board_screw_e_y = 51 + board_offset;
board_screw_f_x = 66 + board_offset;
board_screw_f_y = 35.5 + board_offset;

//lcd
lcd_length = 98;
lcd_width = 60;
lcd_screw_offset = 3;

//base
base_length = board_length + motor_width * 2 + 2 * board_offset;
base_width = board_width + 2 * board_offset;
base_height = 7;
base_screw = 3;
base_screw_holder = 6;
base_screw_holder_height = 3;
base_distance = 70;

//imu
imu_width = 20 + board_offset;
imu_length = 15.5 + board_offset;
imu_screw_a_x = 2.5 + board_offset/2;
imu_screw_a_y = 2.5 + board_offset/2;
imu_screw_b_x = 20 - 2.5 + board_offset/2;
imu_screw_b_y = 2.5 + board_offset/2;
imu_offset = -motor_size/2 - motor_spacing - base_height - base_screw_holder_height;

//battery
battery_width = 40;
battery_mount_size = 20;

//switch
switch_width = 8;
switch_height = 13;

//detector
detector_width = 45 + board_offset;
detector_height = 20 + board_offset;
detector_screw_offset = 1;

module screw(dd=motor_screw){
  cylinder(d=dd, h=base_height*2+base_screw_holder_height);
}
module screw_holder(){
  cylinder(d=base_screw_holder, h=base_screw_holder_height);
}
  
module battery_mount(){
  cube([battery_mount_size, 5, base_height*2]);
}

module motor_holder(){

  difference(){
    hull(){
      size = (motor_size-motor_screw_distance);
      translate([motor_size/2 + motor_screw_distance/2, motor_size/2 + motor_screw_distance/2, 0])cylinder(d=size, h=motor_width);
      translate([motor_size/2 + motor_screw_distance/2, motor_size/2 - motor_screw_distance/2, 0])cylinder(d=size, h=motor_width);
      translate([motor_size/2 - motor_screw_distance/2-size/2-motor_spacing, motor_size/2 + motor_screw_distance/2-size/2, 0])cube([size,size,motor_width]);
      translate([motor_size/2 - motor_screw_distance/2-size/2-motor_spacing, motor_size/2 - motor_screw_distance/2-size/2,0])cube([size,size,motor_width]);
    
    }
    
    //motor center
    translate([motor_size/2, motor_size/2, -motor_width/2])cylinder(d=motor_center, h=motor_width*2);
    
    //screws
    translate([motor_size/2 - motor_screw_distance/2, motor_size/2 - motor_screw_distance/2, -motor_width/2])cylinder(d=motor_screw, h=motor_width*2);
    translate([motor_size/2 + motor_screw_distance/2, motor_size/2 - motor_screw_distance/2, -motor_width/2])cylinder(d=motor_screw, h=motor_width*2);
    translate([motor_size/2 - motor_screw_distance/2, motor_size/2 + motor_screw_distance/2, -motor_width/2])cylinder(d=motor_screw, h=motor_width*2);
    translate([motor_size/2 + motor_screw_distance/2, motor_size/2 + motor_screw_distance/2, -motor_width/2])cylinder(d=motor_screw, h=motor_width*2);
  }
}

module base_bottom(){
  difference(){
    union(){
      translate([-motor_width,0,0])cube([base_length, base_width, base_height]);
      
      translate([board_screw_a_x,board_screw_a_y,base_height])screw_holder();
      translate([board_screw_b_x,board_screw_b_y,base_height])screw_holder();
      translate([board_screw_c_x,board_screw_c_y,base_height])screw_holder();
      translate([board_screw_d_x,board_screw_d_y,base_height])screw_holder();
      translate([board_screw_e_x,board_screw_e_y,base_height])screw_holder();
      translate([board_screw_f_x,board_screw_f_y,base_height])screw_holder();
    }
    
    
    translate([board_screw_a_x,board_screw_a_y,-base_height/2])screw();
    translate([board_screw_b_x,board_screw_b_y,-base_height/2])screw();
    translate([board_screw_c_x,board_screw_c_y,-base_height/2])screw();
    translate([board_screw_d_x,board_screw_d_y,-base_height/2])screw();
    translate([board_screw_e_x,board_screw_e_y,-base_height/2])screw();
    translate([board_screw_f_x,board_screw_f_y,-base_height/2])screw();
  }
}

module base_middle(){
   translate([-motor_width,0,0]){
      cube([base_length, base_width, base_height]);
      translate([0,0,-base_distance])cube([motor_width, base_width, base_distance]);
      translate([base_length-motor_width,0,-base_distance])cube([motor_width, base_width, base_distance]);
}
}

module base_top(){
  difference(){
    translate([-motor_width,0,0]){
      cube([base_length, base_width, base_height]);
      translate([0,0,-base_distance])cube([motor_width, base_width, base_distance]);
      translate([base_length-motor_width,0,-base_distance])cube([motor_width, base_width, base_distance]);
      translate([base_length/2-detector_width/2,0,-detector_height])cube([detector_width, base_height, detector_height]);

      translate([base_length/2-lcd_length/2+lcd_screw_offset,base_width/2-lcd_width/2+lcd_screw_offset,base_height])screw_holder();
      translate([base_length/2+lcd_length/2-lcd_screw_offset,base_width/2-lcd_width/2+lcd_screw_offset,base_height])screw_holder();
      translate([base_length/2-lcd_length/2+lcd_screw_offset,base_width/2+lcd_width/2-lcd_screw_offset,base_height])screw_holder();
      translate([base_length/2+lcd_length/2-lcd_screw_offset,base_width/2+lcd_width/2-lcd_screw_offset,base_height])screw_holder();
    }

    translate([-motor_width,0,0]){
      translate([base_length/2-lcd_length/2+lcd_screw_offset,base_width/2-lcd_width/2+lcd_screw_offset,-base_height/2])screw();
      translate([base_length/2+lcd_length/2-lcd_screw_offset,base_width/2-lcd_width/2+lcd_screw_offset,-base_height/2])screw();
      translate([base_length/2-lcd_length/2+lcd_screw_offset,base_width/2+lcd_width/2-lcd_screw_offset,-base_height/2])screw();
      translate([base_length/2+lcd_length/2-lcd_screw_offset,base_width/2+lcd_width/2-lcd_screw_offset,-base_height/2])screw();

      translate([base_length/2-battery_mount_size/2,base_width/2-battery_width/2,-base_height/2])battery_mount();
      translate([base_length/2-battery_mount_size/2,base_width/2+battery_width/2,-base_height/2])battery_mount();

      translate([-motor_width/2,base_width/2,-base_distance/2])cube([motor_width*2, switch_width, switch_height]);

      translate([base_length/2-detector_width/2+board_offset/2+detector_screw_offset, base_height*2, -detector_height+board_offset/2+detector_screw_offset])rotate([90,0,0])screw(dd=1);
      translate([base_length/2+detector_width/2-board_offset/2-detector_screw_offset, base_height*2, -detector_height+board_offset/2+detector_screw_offset])rotate([90,0,0])screw(dd=1);
      translate([base_length/2-detector_width/2+board_offset/2+detector_screw_offset, base_height*2, -board_offset/2-detector_screw_offset])rotate([90,0,0])screw(dd=1);
      translate([base_length/2+detector_width/2-board_offset/2-detector_screw_offset, base_height*2, -board_offset/2-detector_screw_offset])rotate([90,0,0])screw(dd=1);
    }
  }
}

module imu(){
  difference(){
    union(){
      cube([imu_width, imu_length, base_height]);
      translate([-base_height,0,0])cube([base_height,imu_length, -imu_offset]);
      translate([imu_width,0,0])cube([base_height,imu_length, -imu_offset]);
      translate([imu_screw_a_x,imu_screw_a_y,base_height])screw_holder();
      translate([imu_screw_b_x,imu_screw_b_y,base_height])screw_holder();
    }
    
    translate([imu_screw_a_x,imu_screw_a_y,-base_height/2])screw();
    translate([imu_screw_b_x,imu_screw_b_y,-base_height/2])screw();
  }
}

module motor(){
  color([0,1.0,0]){
    cube([motor_length, motor_size, motor_size]);
    translate([0, motor_size / 2, motor_size / 2])rotate([0,-90,0])cylinder(d = 5, h = 20);
    translate([0, motor_size / 2, motor_size / 2])rotate([0,-90,0])cylinder(d = motor_center, h = 2);
  }
}

//base
base_bottom();
translate([0,0,base_distance])base_middle();
translate([0,0,base_distance*2])base_top();

//imu
translate([base_length / 2 - imu_width / 2 - motor_width,base_width / 2 - imu_length / 2,imu_offset])imu();

//motor holders
translate([-motor_width,base_width/2-motor_size/2,-motor_spacing])rotate([0,90,0])motor_holder();
translate([base_length-motor_width*2,base_width/2-motor_size/2,-motor_spacing])rotate([0,90,0])motor_holder();

//motors
/*
translate([0,base_width/2-motor_size/2,-motor_spacing-motor_size])motor();
translate([base_length-motor_width*2,base_width/2-motor_size/2,-motor_spacing])rotate([0,180,0])motor();
*/
